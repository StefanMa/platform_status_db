# lara_status_db

A simple django database application that can be used to monitor processes and location of labware in a robotic working cell

## Features


## Documentation

The Documentation can be found here: [openlab.gitlab.io/lara_status_db](openlab.gitlab.io/lara_status_db) or [lara_status_db.gitlab.io](lara_status_db.gitlab.io/)


## Credits

This package was created with Cookiecutter* and the `opensource/templates/cookiecutter-pypackage`* project template.

[Cookiecutter](https://github.com/audreyr/cookiecutter )
[opensource/templates/cookiecutter-pypackage](https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage) 
