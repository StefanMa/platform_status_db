import logging
import traceback
from typing import Optional, List, Tuple
import os
import sys
from laborchestrator import structures
import traceback
from django.utils import timezone
from pytz import timezone
import json
from uuid import uuid4

try:
    # add the parent dir of lara_status to the system path, django can import the settings and job_logs
    parent_dir = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
    sys.path.append(parent_dir)
    os.environ['DJANGO_SETTINGS_MODULE'] = 'larastatus.settings'
    from django.core.wsgi import get_wsgi_application

    get_wsgi_application()
    from job_logs.models import (
        Device, Position, Container, ProcessStep, MoveStep, Experiment, Process, Decision, Computation, Result
    )
    from platform_status_db.job_logs.models import (
        Device, Position, Container, ProcessStep, MoveStep, Experiment, Process, Decision, Computation, Result
    )
except ModuleNotFoundError as ex:
    logging.error("Django modules could not be loaded")
except RuntimeError:
    pass
from laborchestrator.database_integration import StatusDBInterface


class StatusDBImplementation(StatusDBInterface):
    def __init__(self):
        super().__init__()
        try:
            # add the parent dir of lara_status to the system path, django can import the settings and job_logs
            sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))
            os.environ['DJANGO_SETTINGS_MODULE'] = 'larastatus.settings'
            from django.core.wsgi import get_wsgi_application
            get_wsgi_application()
            from job_logs.models import (
                Device, Position, Container, ProcessStep, MoveStep, Experiment, Process, Decision, Computation, Result
            )
        except ModuleNotFoundError as ex:
            print("Django modules could not be loaded")
            print(traceback.print_exc())

    def wipe_lara(self):
        Device.objects.all().delete()
        Position.objects.all().delete()
        for cont in Container.objects.all():
            cont.removed = True
            cont.save()

    def create_lara(self):
        Device.objects.create(lara_name="Carousel", num_slots=136)
        Device.objects.create(lara_name="F5", num_slots=1)
        Device.objects.create(lara_name="VarioskanLUX", num_slots=1)
        Device.objects.create(lara_name="Varioskan", num_slots=1)
        Device.objects.create(lara_name="Bravo", num_slots=9)
        Device.objects.create(lara_name="Rotanta", num_slots=4)
        # for the cytomats, the last position is the transfer station
        Device.objects.create(lara_name="Cytomat1550_1", num_slots=33)
        Device.objects.create(lara_name="Cytomat1550_2", num_slots=33)
        Device.objects.create(lara_name="Cytomat470", num_slots=33)
        Device.objects.create(lara_name="Cytomat2C", num_slots=32)
        # the different storage hotels on the platform
        Device.objects.create(lara_name="Hotel_1", num_slots=16)
        Device.objects.create(lara_name="Rotanta_Transfer", num_slots=4)
        Device.objects.create(lara_name="BufferNestsLBravo", num_slots=6)
        Device.objects.create(lara_name="Recovery", num_slots=12)

        deep_well_suited = dict(
            Carousel=[100 + i for i in range(36)],
            F5=[0],
            VarioskanLUX=[],
            Varioskan=[],
            Bravo=[3, 4, 5],
            Cytomat1550_1=[],
            Cytomat1550_2=[],
            Cytomat470=[],
            Cytomat2C=[21 + i for i in range(11)],
            Hotel_1=[0],
            Rotanta_Transfer=[0, 1, 2, 3],
            BufferNestsLBravo=[0],
            Recovery=[0],
            Rotanta=[0, 1, 2, 3],
        )
        for device in Device.objects.all():
            for i in range(device.num_slots):
                Position.objects.create(
                    device=device,
                    slot_number=i,
                    deep_well_suited=i in deep_well_suited[device.lara_name])

    def get_all_positions(self, device: str) -> List[int]:
        positions = Position.objects.filter(device__lara_name=device)
        indices = [p.slot_number for p in positions]
        return indices

    def add_process_to_db(self, name: str, src: str) -> str:
        uuid = str(uuid4())
        Process.objects.create(
            name=name,
            pythonlab_description=src,
            process_uuid=uuid,
        )
        return uuid

    def get_available_processes(self) -> List[Tuple[str, str]]:
        available_processes = []
        # get all from the database
        for process in Process.objects.all():
            available_processes.append((
                process.name,
                process.process_uuid
            ))
        return available_processes

    def get_process(self, process_id: str) -> str:
        process = Process.objects.get(process_uuid=process_id)
        return process.pythonlab_description

    def create_experiment(self, process_id: str) -> str:
        process = Process.objects.get(process_uuid=process_id)
        experiment_uuid = str(uuid4())
        Experiment.objects.create(
            experiment_uuid=experiment_uuid,
            process=process
        )
        return experiment_uuid

    def position_empty(self, device: str, pos: int) -> bool:
        try:
            pos = Position.objects.get(slot_number=pos, device__lara_name=device)
            if pos.container_set.filter(removed=False):
                return False
            if pos.lid_pos.filter(removed=False):
                return False
            return True
        except Exception as ex:
            logging.warning(f"Could not get information about {pos} of device {device} from database."
                            f" Returning False (means not empty)\n{ex, traceback.print_exc()}")
            return True

    def get_container_at_position(self, device: str, pos: int) -> Optional[structures.ContainerInfo]:
        try:
            source_position = Position.objects.get(slot_number=pos, device__lara_name=device)
            # find the container
            cont = Container.objects.get(removed=False, current_pos=source_position)
            if cont.lid_pos:
                lid_site = [cont.lid_pos.device, cont.lid_pos.slot_number]
            else:
                lid_site = None
            cont_info = structures.ContainerInfo(name='Name',
                                                 current_device=cont.current_pos.device.lara_name,
                                                 current_pos=cont.current_pos.slot_number,
                                                 barcode=cont.barcode,
                                                 start_device=cont.starting_pos.device,
                                                 lidded=cont.lidded,
                                                 lid_site=lid_site,
                                                 filled=True)  # todo: save that in the database (but true is safer)
            return cont_info
        except:
            return None

    def moved_container(self, source_device: str, source_pos: int, target_device: str, target_pos: int):
        logging.info(
            f"Moving container from {source_device}[{source_pos}] to {target_device}[{target_pos}] in database")
        try:
            # find the containers current position
            source_position = Position.objects.get(slot_number=source_pos, device__lara_name=source_device)
            # find the container
            container = Container.objects.get(removed=False, current_pos=source_position)
            # find the new position
            target_position = Position.objects.get(slot_number=target_pos, device__lara_name=target_device)
            # update the current position
            container.current_pos = target_position
            container.save()
        except Exception as ex:
            logging.warning(
                f"Could not save movement from {source_device}[{source_pos}] to {target_device}[{target_pos}],"
                f" {ex}\n{traceback.print_exc()}")

    def unlidded_container(self, cont_info: structures.ContainerInfo, lid_device: str, lid_pos: int):
        try:
            # get the container entry from the database
            cont = self._get_container(cont_info)
            pos = Position.objects.get(slot_number=lid_pos, device__lara_name=lid_device)
            cont.lid_pos = pos
            cont.lidded = False
            cont.save()
        except Exception as ex:
            logging.error(f"Saving the unlidding of {cont_info} with putting lid to "
                          f"{(lid_device, lid_pos)} failed: {ex}\n{traceback.print_exc()}")

    def lidded_container(self, cont_info: structures.ContainerInfo,
                         lid_device: Optional[str] = None, lid_pos: Optional[int] = None):
        try:
            # get the container entry from the database
            cont = self._get_container(cont_info)
            if lid_device and lid_pos:
                try:
                    pos = Position.objects.get(slot_number=lid_pos, device__lara_name=lid_device)
                    assert pos == cont.lid_pos
                except Exception as ex:
                    logging.warning(f"Lid verification ({lid_device}|{lid_pos}) failed: {ex}")
            cont.lid_pos = None
            cont.lidded = True
            cont.save()
        except Exception as ex:
            logging.error(f"Saving the lidding of {cont_info} with lid {(lid_device, lid_pos)} failed: {ex}\n"
                          f"{traceback.print_exc()}")

    def get_cont_info_by_barcode(self, barcode: str) -> structures.ContainerInfo:
        try:
            cont = Container.objects.get(removed=False, barcode=barcode)
            if cont.lid_pos:
                lid_site = [cont.lid_pos.device, cont.lid_pos.slot_number]
            else:
                lid_site = None
            cont_info = structures.ContainerInfo(name='Name',
                                                 current_device=cont.current_pos.device.lara_name,
                                                 current_pos=cont.current_pos.slot_number,
                                                 barcode=barcode,
                                                 start_device=cont.starting_pos.device,
                                                 lidded=cont.lidded,
                                                 filled=True,  # this is safer until its handled properly
                                                 lid_site=lid_site)
            return cont_info
        except Exception as ex:
            logging.error(f"error while retrieving info about container with barcode {barcode}")

    def add_container(self, cont: structures.ContainerInfo):
        try:
            position = Position.objects.get(slot_number=cont.current_pos, device__lara_name=cont.current_device)
            Container.objects.create(
                current_pos=position,
                starting_pos=position,
                barcode=cont.barcode,
                lidded=cont.lidded,
                labware_uuid="D" * 32,
            )
        except Exception as ex:
            print(f"Could not add Container{cont}: {ex}")
            print(traceback.print_exc())

    def remove_container(self, cont: structures.ContainerInfo):
        try:
            container = self._get_container(cont)
            container.removed = True
            container.save()
        except Exception as ex:
            print(f"Could not remove Container{cont}: {ex}")
            print(traceback.print_exc())

    def _get_container(self, container_info: structures.ContainerInfo) -> Optional[Container]:
        """
        Tries to retrieve the container from the database using the barcode.
        If that doesn't work, it tries the position.
        If that does not work, it returns None
        """
        try:
            container = Container.objects.get(removed=False, barcode=container_info.barcode)
            return container
        except:
            try:
                position = Position.objects.get(slot_number=container_info.current_pos,
                                                device__lara_name=container_info.current_device)
                # find the container
                container = Container.objects.get(removed=False, current_pos=position)
                return container
            except:
                return None

    def set_barcode(self, cont: structures.ContainerInfo):
        try:
            container = self._get_container(cont)
            container.barcode = cont.barcode
            container.save()
        except Exception as ex:
            logging.error(f"could not set barcode of container {cont}: {ex} \n{traceback.print_exc()}")

    def update_lid_position(self, cont: structures.ContainerInfo):
        try:
            # find the container
            container = self._get_container(cont)
            if not cont.lidded and cont.lid_site is not None:
                # find the containers current lid position
                position = Position.objects.get(slot_number=cont.lid_site[1], device__lara_name=cont.lid_site[0])
                container.lid_pos = position
            else:
                container.lid_pos = None
            container.lidded = cont.lidded
            container.save()
        except Exception as ex:
            print(f"could not update lid position of {cont}: {ex}")
            print(traceback.print_exc())

    def get_estimated_duration(self, step: structures.ProcessStep, confidence=.95) -> Optional[float]:
        past_steps = ProcessStep.objects.all()
        command = step.data['fct']
        durations = []
        for past_step in past_steps:
            try:
                params = json.loads(past_step.parameters)
                similar = False
                if params['fct'] == command:
                    if isinstance(step, structures.MoveStep):
                        similar = self._movement_match(step, past_step.movestep)
                    if command == 'read_barcode':
                        similar = True
                    elif command in ['absorbance', 'executeProtocol']:
                        similar = self._protocol_match(step, past_step, params)
                if similar:
                    durations.append(past_step.get_duration())
            except Exception as ex:  # we just ignore all entries that make problems. We do not need them anyway
                print(ex, traceback.print_exc())
        if durations:
            print(command, "past: ", durations)
            estimate = max(durations)
            return estimate
        return None

    def get_estimated_durations(self, steps: List[structures.ProcessStep], confidence=.95) -> List[Optional[float]]:
        past_steps = list(ProcessStep.objects.all())
        params_list = [json.loads(past_step.parameters) for past_step in past_steps]
        guesses = []
        for new_step in steps:
            command = new_step.data['fct']
            durations = []
            for past_step, params in zip(past_steps, params_list):
                try:
                    similar = False
                    if params['fct'] == command:
                        if isinstance(new_step, structures.MoveStep):
                            similar = self._movement_match(new_step, past_step.movestep)
                        elif command == 'read_barcode':
                            similar = True
                        elif command in ['absorbance', 'executeProtocol']:
                            similar = self._protocol_match(new_step, past_step, params)
                        elif command == 'read_series':
                            similar = self._protocol_series_match(new_step, past_step, params)
                        else:
                            similar = self._arbitrary_match(new_step, past_step, params)
                    if similar:
                        durations.append(past_step.get_duration())
                except Exception as ex:  # we just ignore all entries that make problems. We do not need them anyway
                    print(ex, traceback.print_exc())
            if durations:
                logging.debug(command, "past: ", durations)
                estimate = max(durations)
                guesses.append(estimate)
            else:
                guesses.append(None)
        if not len(guesses) == len(steps):
            logging.error(f"we got {len(guesses)} guesses to {len(steps)} new steps... that's wrong")
            return [None] * len(steps)
        return guesses

    @staticmethod
    def _movement_match(new_step: structures.MoveStep, past_step: MoveStep) -> bool:
        # compares the names of origin and destination
        if new_step.origin_device.name:
            origin_name = new_step.origin_device.name
        else:
            origin_name = new_step.origin_device.preferred
        if new_step.target_device.name:
            target_name = new_step.origin_device.name
        else:
            target_name = new_step.target_device.preferred
        return origin_name == past_step.origin.device.lara_name and \
            target_name == past_step.destination.device.lara_name

    @staticmethod
    def _protocol_match(new_step: structures.ProcessStep, past_step: ProcessStep, past_params) -> bool:
        method = new_step.data['method']
        past_method = past_params['method']
        return method == past_method

    @staticmethod
    def _protocol_series_match(new_step: structures.ProcessStep, past_step: ProcessStep, past_params) -> bool:
        new_protocols = new_step.data['protocols']
        past_protocols = past_params['protocols']
        return len(new_protocols) == len(past_protocols) \
            and all(new == old for new, old in zip(new_protocols, past_protocols))

    @staticmethod
    def _arbitrary_match(new_step: structures.ProcessStep, past_step: ProcessStep, past_params) -> bool:
        """ Check whether all parameters match """
        for key, val in new_step.data.items():
            if key not in past_params:
                return False
            elif not val == past_params[key]:
                return False
        return True

    def safe_step_to_db(self, step: structures.ProcessStep, container_info: structures.ContainerInfo,
                        experiment_uuid: str):
        try:
            berlin = timezone('europe/berlin')
            container = self._get_container(container_info)
            if step.status == structures.StepStatus.FINISHED:
                experiment = Experiment.objects.get(experiment_uuid=experiment_uuid)
                kwargs = dict(
                    start=berlin.localize(step.start),
                    finish=berlin.localize(step.finish),
                    container=container,
                    executing_device=Device.objects.get(lara_name=step.main_device.name),
                    experiment=experiment,
                    process_name='Test_Process',
                    is_simulation=True,
                    parameters=json.dumps(step.data, default=str),
                )
                if isinstance(step, structures.MoveStep):
                    # todo this is all wrong. the actual movement was written by the arm server into the database
                    kwargs['origin'] = container.current_pos
                    target = Position.objects.get(slot_number=step.destination_pos,
                                                  device__lara_name=step.target_device.name)
                    kwargs['destination'] = target
                    kwargs['lidded_before'] = True
                    kwargs['lidded_after'] = True
                    kwargs['barcode_read'] = False
                    MoveStep.objects.create(**kwargs)
                else:
                    ProcessStep.objects.create(**kwargs)
        except Exception as ex:
            print(f"Could not save job {step.name} ({ex})")
            print(traceback.print_exc())
