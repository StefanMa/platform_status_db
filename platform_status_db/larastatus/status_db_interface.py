"""
The formal interface a database must implement to enable
"""
from typing import Optional
from scheduling_module import structures
from abc import ABC, abstractmethod


class StatusDBInterface(ABC):
    def __init__(self):
        pass

    @abstractmethod
    def moved_container(self, source_device: str, source_pos: int, target_device: str, target_pos: int):
        """
        Saves a movement of a container (specified by its prior position) to the database
        :param source_device:
        :param source_pos:
        :param target_device:
        :param target_pos:
        :return:
        """

    @abstractmethod
    def unlidded_container(self, lid_device: str, lid_pos: int, cont_device: str, cont_pos: int):
        pass


    @abstractmethod
    def lidded_container(self, lid_device: str, lid_pos: int, cont_device: str, cont_pos: int):
        pass

    @abstractmethod
    def get_cont_info_by_barcode(self, barcode: str) -> structures.ContainerInfo:
        """
        Retrieves all available information for the container with the given barcode
        :param barcode:
        :return:
        """

    @abstractmethod
    def add_container(self, cont: structures.ContainerInfo):
        """
        Adds a container to the database with its starting position as the current position.
        :param cont: The container to add
        :return: nothin
        """

    @abstractmethod
    def remove_container(self, cont: structures.ContainerInfo):
        """
        Marks the given container as removed from the platform. The information is still kept in the database
        :param cont: The container to mark removed
        :return: nothing
        """

    @abstractmethod
    def set_barcode_at_position(self, barcode: str, device: str, pos: int):
        """
        Sets the barcode to a container that is specified by its current position.
        :param barcode:
        :param device:
        :param pos:
        :return:
        """

    @abstractmethod
    def set_barcode(self, cont: structures.ContainerInfo):
        """
        Sets the barcode of an existing container. Assumes the barcode is already saved in the ContainerInfo.
        :param cont:
        :return:
        """

    @abstractmethod
    def update_lid_position(self, cont: structures.ContainerInfo):
        """
        Sets the barcode of an existing container. Assumes the barcode is already saved in the ContainerInfo.
        :param cont:
        :return:
        """

    @abstractmethod
    def get_estimated_duration(self, job: structures.Job, confidence=.95) -> Optional[float]:
        """
        Checks the database for similar jobs to estimate the duration of a job.
        :param job: the job of which the duration shall be estimated
        :param confidence: chance, that the actual duration is less or equal the estimated duration.
        :return: duration in seconds or None if no information was found in the database
        """

    @abstractmethod
    def safe_job_to_db(self, job: structures.Job, container_info: structures.ContainerInfo):
        """
        Saves a finished job to the database. It automatically recognizes move jobs.
        :param container_info: information about the processed container
        :param job: the structures.Job to save
        :return: nothing
        """
