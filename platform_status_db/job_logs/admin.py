from django.contrib import admin

from . import models
from django.db.models import Model

# register all Models
for attr in dir(models):
    elem = getattr(models, attr)
    try:
        if issubclass(elem, Model):
            admin.site.register(elem)
    except:
        pass
