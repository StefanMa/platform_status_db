
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.template import loader
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views import generic
from django.utils import timezone

from .models import Device, Container, Position, ProcessStep, MoveStep


def main_index(request):
    context = {'sub_indices': ['devices']}
    response = render(request, 'index.html', context)
    return response


def movement_finder(request):
    response = render(request, 'movement_finder.html')
    return response


class IndexView(generic.ListView):
    template_name = "deviceindex.html"
    context_object_name = 'list'

    def get_queryset(self):
        return Device.objects.all()


class DeviceView(generic.DetailView):
    model = Device
    template_name = 'device_detail.html'
    context_object_name = 'device'


class LabwareView(generic.ListView):
    template_name = 'labware_detail.html'
    context_object_name = 'list'

    def get_queryset(self):
        return Device.objects.all()
        #return Container.objects.filter(removed=False)


class PositionView(generic.DetailView):
    model = Position
    template_name = 'position_detail.html'
    context_object_name = 'position'


class ContainerView(generic.DetailView):
    model = Container
    template_name = 'container_detail.html'
    context_object_name = 'container'


class JobView(generic.DetailView):
    model = ProcessStep
    template_name = 'job_detail.html'
    context_object_name = 'job'


class MoveJobView(generic.DetailView):
    model = MoveStep
    template_name = 'move_job_detail.html'
    context_object_name = 'move_job'
