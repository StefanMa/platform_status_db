from django.test import TestCase
from .import models


class LaraJobTest(TestCase):

    def test_job_save(self):
        models.Device.objects.create(lara_name="Bravo", num_slots=6)
        models.Device.objects.create(lara_name="Carousel", num_slots=150)
        models.Device.objects.create(lara_name="Cytomat1550_1", num_slots=32)
