from django.urls import path

from . import views

app_name = 'job_logs'
urlpatterns = [
    path('', views.main_index, name='index'),
    path('devices/', views.IndexView.as_view(), name='devices'),
    path('movement_finder/', views.movement_finder, name='movement_finder'),
    path('present_labware/', views.LabwareView.as_view(), name='present_labware'),
    path('devices/<int:pk>/', views.DeviceView.as_view(), name='device_detail'),
    path('positions/<int:pk>/', views.PositionView.as_view(), name='position_detail'),
    path('containers/<int:pk>/', views.ContainerView.as_view(), name='container_detail'),
    path('jobs/<int:pk>/', views.JobView.as_view(), name='job_detail'),
    path('move_jobs/<int:pk>/', views.MoveJobView.as_view(), name='move_job_detail'),
]
