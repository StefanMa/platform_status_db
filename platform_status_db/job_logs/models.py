from __future__ import annotations
from django.db import models
from datetime import datetime, timedelta


class Device(models.Model):
    # app_label = 'job_logs'
    num_slots = models.IntegerField(default=1)
    lara_name = models.CharField(max_length=50)
    lara_uri = models.URLField()

    def __str__(self):
        return self.lara_name


class Position(models.Model):
    device = models.ForeignKey(Device, on_delete=models.CASCADE)
    slot_number = models.IntegerField()
    deep_well_suited = models.BooleanField(default=False)

    def __str__(self):
        return f'{str(self.device)}[{self.slot_number}]'


class Container(models.Model):
    current_pos = models.ForeignKey(Position, on_delete=models.CASCADE)
    starting_pos = models.ForeignKey(Position, on_delete=models.CASCADE, related_name='starting_pos')
    barcode = models.CharField(max_length=50)
    lidded = models.BooleanField('currently lidded')
    # in case there is no lid or the container is currently lidded, the following field is nulled
    lid_pos = models.ForeignKey(Position, on_delete=models.CASCADE, blank=True, null=True, default=None,
                                related_name='lid_pos')
    removed = models.BooleanField('got removed from platform', default=False)
    labware_uuid = models.UUIDField()

    def __str__(self):
        return f"Container({self.barcode})"


class Process(models.Model):
    name = models.CharField(max_length=200)
    pythonlab_description = models.TextField()
    process_uuid = models.UUIDField()


class Experiment(models.Model):
    experiment_uuid = models.UUIDField()
    process = models.ForeignKey(Process, on_delete=models.CASCADE)


class ProcessStep(models.Model):
    start = models.DateTimeField('job started')
    finish = models.DateTimeField('job finished')
    executing_device = models.ForeignKey(Device, on_delete=models.CASCADE)
    container = models.ForeignKey(Container, on_delete=models.CASCADE, blank=True, null=True, default=None)
    experiment = models.ForeignKey(Experiment, null=True, on_delete=models.CASCADE)
    process_name = models.CharField(max_length=200)
    is_simulation = models.BooleanField(default=True)
    # this will contain a json-string representing the kwargs with their args converted to str
    parameters = models.TextField(default='{}')

    def get_duration(self) -> float:
        # magically get converted to datetime
        return (self.finish - self.start).total_seconds()

    def __str__(self):
        return f"Step_{self.executing_device}_(process={self.process_name})"


class MoveStep(ProcessStep):
    origin = models.ForeignKey(Position, on_delete=models.CASCADE, related_name='origin')
    destination = models.ForeignKey(Position, on_delete=models.CASCADE, related_name='destination')
    lidded_before = models.BooleanField()
    lidded_after = models.BooleanField()
    barcode_read = models.BooleanField('read barcode on the way')
    uri_format = models.CharField(max_length=400)

    def __str__(self):
        return f"{str(self.origin)}-->{str(self.destination)}"


class Result(models.Model):
    step = models.ForeignKey(ProcessStep, on_delete=models.CASCADE)
    content = models.TextField()


class Decision(models.Model):
    pass


class Computation(models.Model):
    pass
