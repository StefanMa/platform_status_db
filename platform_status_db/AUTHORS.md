
# Acknowledgements and Credits

The lara_status_db project thanks


Contributors
------------

* Mickey Kim <mickey.kim@genomicsengland.co.uk>  ! Thanks for the phantastic cookiecutter template !


Development Lead
----------------

* stefan maak <maaks@uni-greifswald.de>