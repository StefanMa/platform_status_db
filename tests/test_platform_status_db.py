#!/usr/bin/env python
"""Tests for `platform_status_db` package."""
# pylint: disable=redefined-outer-name
# from platform_status_db import __version__
from platform_status_db.larastatus.status_db_implementation import StatusDBImplementation
from laborchestrator.database_integration.status_db_interface import StatusDBInterface
from laborchestrator.structures import ContainerInfo, UsedDevice, MoveStep, StepStatus
import os
from datetime import datetime, timedelta
from numpy import random
import time
import sys


def test_version():
    """Sample pytest test function."""
    # assert __version__ == "0.0.1"


def test_Interface():
    """ testing the formal interface (GreeterInterface)
    """
    assert issubclass(StatusDBImplementation, StatusDBInterface)


def test_Import():
    # add the parent dir of lara_status to the system path, django can import the settings and job_logs
    sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))
    os.environ['DJANGO_SETTINGS_MODULE'] = 'larastatus.settings'
    from django.core.wsgi import get_wsgi_application
    get_wsgi_application()
    from job_logs import models


def test_Experiment_creation():
    """ Testing HelloWorld class
    """
    try:
        # add the parent dir of lara_status to the system path, django can import the settings and job_logs
        sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))
        os.environ['DJANGO_SETTINGS_MODULE'] = 'larastatus.settings'
        from django.core.wsgi import get_wsgi_application
        get_wsgi_application()
        from job_logs import models
        db_client = StatusDBImplementation()
        # create dummy_process
        process_uuid = db_client.add_process_to_db("Dummy_process", "Lorem ipsum sit dolor est")
        # create three dummy_devices
        dummy_devices = [
            models.Device.objects.create(lara_name="Dummy_device_1", num_slots=1),
            models.Device.objects.create(lara_name="Dummy_mover", num_slots=1),
            models.Device.objects.create(lara_name="Dummy_device_2", num_slots=1),
        ]
        # create the positions within
        for device in dummy_devices:
            models.Position.objects.create(
                device=device,
                slot_number=0,
                deep_well_suited=False)
        # create dummy_experiment from dummy_process
        experiment_uuid = db_client.create_experiment(process_id=process_uuid)
        # create dummy_labware
        barcode = f"barcode_{random.randint(99999)}"
        container = ContainerInfo(name='Dummy_container', current_device="Dummy_device_1", current_pos=0,
                                  barcode=barcode,
                                  start_device=UsedDevice(device_type='Dummy', name='Dummy_device_1'))
        db_client.add_container(container)
        # save a movement from one dummy device to the other
        now = datetime.today()
        soon = now + timedelta(seconds=1)
        dummy_movement = MoveStep(name="Dummy_move", cont_names=['Dummy_container'], function='move', duration=.1,
                                  start=now, finish=soon,
                                  process_name=experiment_uuid, used_devices=[
                                      UsedDevice(device_type="Dummy", name='Dummy_device_1', tag="origin"),
                                      UsedDevice(device_type="Dummy", name='Dummy_mover', tag="main"),
                                      UsedDevice(device_type="Dummy", name='Dummy_device_2', tag="target"),
                                  ])
        dummy_movement.status = StepStatus.FINISHED
        db_client.safe_step_to_db(dummy_movement, container, experiment_uuid=experiment_uuid)
        db_client.moved_container("Dummy_device_1", 0, "Dummy_device_2", 0)
        # check whether the plate arrived
        position = models.Position.objects.get(slot_number=0, device__lara_name="Dummy_device_2")
        # assert the container is there
        position.container_set.get(barcode=barcode)
        # assert the job was saved
        models.MoveStep.objects.get(origin__device=dummy_devices[0], destination__device=dummy_devices[2])
    finally:
        # delete all dummy_stuff
        for device in dummy_devices:
            device.delete()
        for process in models.Process.objects.filter(name='Dummy_process'):
            process.delete()
