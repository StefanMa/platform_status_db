# Platform Status DB

A django implementation to meet the needs (implement the interface) for the PythonLanOrchestrator, i.e. a database managing process logging, container locations, etc.

## Features


## Documentation

The Documentation can be found here: [openlab.gitlab.io/platform-status-db](openlab.gitlab.io/platform-status-db) or [platform-status-db.gitlab.io](platform_status_db.gitlab.io/)


## Credits

This package was created with Cookiecutter* and the `opensource/templates/cookiecutter-pypackage`* project template.

[Cookiecutter](https://github.com/audreyr/cookiecutter )
[opensource/templates/cookiecutter-pypackage](https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage) 
