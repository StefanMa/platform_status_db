.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Platform Status DB, run this command in your terminal:

.. code-block:: console

    $ pip install platform_status_db

This is the preferred method to install Platform Status DB, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From source
-----------

