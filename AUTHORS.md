
# Acknowledgements and Credits

The Platform Status DB project thanks


Contributors
------------

* Mickey Kim <mickey.kim@genomicsengland.co.uk>  ! Thanks for the phantastic cookiecutter template !


Development Lead
----------------

* stefan maak <maaks@uni-greifswald.de>